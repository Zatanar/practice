import type { Knex } from "knex";
import * as dotenv from 'dotenv'
dotenv.config();
import configuration from './src/config/configuration';

const config: { [key: string]: Knex.Config } = {
  development: {
      client: 'postgresql',
      useNullAsDefault: true,
      connection: configuration().POSTGRES_PATH,
      migrations: {
          directory: './src/database/migrations',
      },
      seeds:{
          directory: './src/database/seeds',
      }
  },

  staging: {
    client: "postgresql",
    connection: configuration().POSTGRES_PATH,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  },

  production: {
    client: "postgresql",
    connection: configuration().POSTGRES_PATH,
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  }

};

module.exports = config;

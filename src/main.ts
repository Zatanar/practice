import { NestFactory, Reflector } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { postgraphile } from 'postgraphile';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
    whitelist: true,
  }));
  app.useGlobalInterceptors(new ClassSerializerInterceptor(
    app.get(Reflector),
  ));
  const configService = app.get<ConfigService>(ConfigService);

  app.use(postgraphile(
    configService.get('POSTGRES_PATH'),
    'public',
    {
      disableDefaultMutations: true,
      // eslint-disable-next-line global-require
      appendPlugins: [require('@graphile-contrib/pg-simplify-inflector')],
      watchPg: true,
      graphiql: process.env.NODE_ENV !== 'development',
      enhanceGraphiql: true,
      graphqlRoute: '/pggraphql',
      graphiqlRoute: '/graphiql',
      exportGqlSchemaPath: './pgschema.gql',
      simpleCollections: 'both',
      graphileBuildOptions: {
        pgDeletedColumnName: 'deleted_at',
        pgDeletedRelations: true,
        connectionFilterRelations: true,
        /*
             * Uncomment if you want simple collections to lose the 'List' suffix
             * (and connections to gain a 'Connection' suffix).
             */
        pgOmitListSuffix: true,
      },
    },
  ));
  await app.listen(configService.get('APP_PORT'));
}
bootstrap();

import {
  Args, ID, Mutation, Query, Resolver, Int,
} from '@nestjs/graphql';
import { ParseUUIDPipe, UseGuards } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UserEntity } from './entities/user.entity';
import { UsersService } from './users.service';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';
import { GqlAuthGuard } from '../auth/jwt-auth.guard';

@Resolver(UserEntity)
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Query(() => UserEntity)
  async neverUseFunction() {
    return 0;
  }

  // id is uuid
  // @Query(() => UserEntity)
  // async findUserById(@Args('id', { type: () => ID }, ParseUUIDPipe) id:string): Promise<UserEntity> {
  //   return this.usersService.findUserById(id);
  // }
  //
  // @Query(() => [UserEntity])
  // async findAllUsers(): Promise<UserEntity[]> {
  //   return this.usersService.findAllUsers();
  // }

  // TODO Хак для удобства, удалить
  @Mutation(() => UserEntity)
  async createUser(@Args('input') input: CreateUserInput) :Promise<UserEntity> {
    input.password = await bcrypt.hash(input.password, 10);
    return this.usersService.createUser(input);
  }

  // id is uuid
  @Mutation(() => Int)
  async deleteUser(@Args('id', { type: () => ID }, ParseUUIDPipe) id:string): Promise<number> {
    return this.usersService.deleteUser(id);
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => Int)
  async updateUser(@Args('update') input: UpdateUserInput) :Promise<number> {
    return this.usersService.updateUser(input);
  }
}

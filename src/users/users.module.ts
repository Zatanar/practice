import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UsersResolver } from './users.resolver';
import { GoodsService } from '../goods/goods.service';

@Module({
  controllers: [UsersController],
  providers: [UsersService,
    UsersResolver,
    GoodsService,
  ],
  exports: [UsersService],
})
export class UsersModule {
}

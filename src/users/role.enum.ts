import { registerEnumType } from '@nestjs/graphql';

enum Role {
  User = 'user',
  Admin = 'admin',
}
registerEnumType(Role, {
  name: 'Role',
});
export default Role;

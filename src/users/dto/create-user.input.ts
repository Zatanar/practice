import { Field, InputType } from '@nestjs/graphql';
import { IsString, Length } from 'class-validator';
import Role from '../role.enum';

@InputType()
export class CreateUserInput {
  @IsString()
  @Length(3, 20)
  @Field(() => String)
    name: string;

  @IsString()
  @Length(7)
  @Field(() => String)
    password: string;

  roles?: Role[];
}

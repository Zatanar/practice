import {
  Field, ID, InputType, PartialType,
} from '@nestjs/graphql';
import { IsUUID } from 'class-validator';
import { CreateUserInput } from './create-user.input';

@InputType()
export class UpdateUserInput extends PartialType(CreateUserInput) {
  @IsUUID('4')
  @Field(() => ID)
    id: string;
}

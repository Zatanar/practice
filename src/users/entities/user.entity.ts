import { Model } from 'objection';
import {
  Field, HideField, ID, ObjectType,
} from '@nestjs/graphql';
import { Exclude } from 'class-transformer';
import { GoodEntity } from '../../goods/entities/good.entity';
import Role from '../role.enum';

@ObjectType()
export class UserEntity extends Model {
  static get tableName() {
    return 'users';
  }

  // id is uuid
  @Field((type) => ID)
    id: string;

  // @Field((type) => String)
  @Exclude()
  @HideField()
    password: string;

  @Field((type) => String)
    name: string;

  @Field((type) => [GoodEntity])
    goods: GoodEntity[];

  @Field((type) => Role)
    roles: Role[];

  static relationMappings = () => ({
    goods: {
      relation: Model.HasManyRelation,
      modelClass: GoodEntity,
      join: {
        from: 'users.id',
        to: 'goods.userId',
      },
    },
  });
}

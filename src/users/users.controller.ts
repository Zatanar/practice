import {
  Controller, Get, UseGuards,
} from '@nestjs/common';
import { UsersService } from './users.service';
import Role from './role.enum';
import RoleGuard from '../auth/role.guard';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @UseGuards(RoleGuard(Role.Admin))
  @Get()
  async findAllUsers() {
    return this.usersService.findAllUsers();
  }
}

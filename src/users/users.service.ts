import { Injectable } from '@nestjs/common';
import { UserEntity } from './entities/user.entity';
import { CreateUserInput } from './dto/create-user.input';
import { UpdateUserInput } from './dto/update-user.input';

@Injectable()
export class UsersService {
  // id is uuid
  async findUserById(id:string): Promise<UserEntity> {
    return UserEntity.query().findById(id).withGraphFetched('goods');
  }

  async findUserByName(name: string): Promise<UserEntity> {
    return UserEntity.query().findOne({ name });
  }

  async findAllUsers(): Promise<UserEntity[]> {
    return UserEntity.query().withGraphFetched('goods');
  }

  async createUser(input: CreateUserInput): Promise<UserEntity> {
    return UserEntity.query().insert(input);
  }

  // id is uuid
  async deleteUser(id: string): Promise<number> {
    return UserEntity.query().deleteById(id);
  }

  async updateUser(input: UpdateUserInput): Promise<number> {
    return UserEntity.query().where({ id: input.id }).update(input);
  }
}

import { Knex } from 'knex';
import * as bcrypt from 'bcrypt';

export async function seed(knex: Knex): Promise<void> {
  await knex('users').del();

  const hashedPassword = await bcrypt.hash('password', 10);

  await knex('users').insert([
    {
      name: 'admin',
      password: hashedPassword,
      roles: 'admin',
    },
  ]);
}

import { Knex } from 'knex';
import { UserEntity } from '../../users/entities/user.entity';
import { GoodEntity } from '../../goods/entities/good.entity';

export async function up(knex: Knex): Promise<void> {
  const goodTableExists = await knex.schema.hasTable(GoodEntity.tableName);
  if (!goodTableExists) {
    await knex.schema.createTable(GoodEntity.tableName, (table) => {
      table
        .uuid('id')
        .primary()
        .notNullable()
        .defaultTo(knex.raw('uuid_generate_v4()'));

      table
        .uuid('user_id')
        .references('id')
        .inTable(UserEntity.tableName)
        .notNullable();

      table
        .float('cost')
        .notNullable();
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  const goodTableExists = await knex.schema.hasTable(GoodEntity.tableName);
  if (goodTableExists) {
    await knex.schema.dropTable(GoodEntity.tableName);
  }
}

import { Knex } from 'knex';
import { UserEntity } from '../../users/entities/user.entity';

export async function up(knex: Knex): Promise<void> {
  await knex.raw('CREATE EXTENSION IF NOT EXISTS "uuid-ossp"');
  await knex.schema.alterTable(UserEntity.tableName, (table) => {
    table.dropPrimary();
    table
      .uuid('uuid')
      .primary()
      .notNullable()
      .defaultTo(knex.raw('uuid_generate_v4()'));
  });
  await knex.schema.alterTable(UserEntity.tableName, (table) => {
    table.dropColumn('id');
    table.renameColumn('uuid', 'id');
  });
}
export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable(UserEntity.tableName, (table) => {
    table.dropPrimary();
  });
  await knex.schema.alterTable(UserEntity.tableName, (table) => {
    table
      .increments('new_id')
      .primary()
      .notNullable();
  });
  await knex.schema.alterTable(UserEntity.tableName, (table) => {
    table.dropColumn('id');
    table.renameColumn('new_id', 'id');
  });
}

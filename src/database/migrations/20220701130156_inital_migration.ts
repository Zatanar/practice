import { Knex } from 'knex';
import { UserEntity } from '../../users/entities/user.entity';

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createSchemaIfNotExists('public');
}

export async function down(knex: Knex): Promise<void> {
  const userTableExists = await knex.schema.hasTable(UserEntity.tableName);
  // Обратная совместимость, когда в этой миграции создавалась таблица users
  if (userTableExists) {
    await knex.schema
      .dropTable(UserEntity.tableName);
  }
}

import { Knex } from 'knex';
import { UserEntity } from '../../users/entities/user.entity';
import Role from '../../users/role.enum';

export async function up(knex: Knex): Promise<void> {
  const userTableExists = await knex.schema.hasTable(UserEntity.tableName);
  if (!userTableExists) {
    await knex.schema.createTable(UserEntity.tableName, (table) => {
      table
        .increments('id')
        .primary()
        .notNullable();
      table
        .string('name')
        .notNullable()
        .unique();
      table
        .string('password')
        .notNullable()
        .unique();
      table
        .enum('roles', [Role.User, Role.Admin])
        .notNullable()
        .defaultTo(Role.User);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  const userTableExists = await knex.schema.hasTable(UserEntity.tableName);
  if (userTableExists) {
    await knex.schema
      .dropTable(UserEntity.tableName);
  }
}

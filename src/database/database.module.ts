import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import Knex from 'knex';
import { knexSnakeCaseMappers, Model } from 'objection';
import { UserEntity } from '../users/entities/user.entity';
import { GoodEntity } from '../goods/entities/good.entity';

const models = [UserEntity, GoodEntity];

const modelProviders = models.map((model) => ({
  provide: model.name,
  useValue: model,
}));

const providers = [
  ...modelProviders,
  {
    provide: 'KnexConnection',
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: async (config: ConfigService) => {
      const knex = Knex({
        client: 'postgresql',
        connection: config.get('POSTGRES_PATH'),
        ...knexSnakeCaseMappers(),
      });

      Model.knex(knex);
      return knex;
    },
  },
];

@Global()
@Module({
  providers: [...providers],
  exports: [...providers],
})
export class DatabaseModule {}

import {
  IsString, MinLength, Length,
} from 'class-validator';

export class RegisterDto {
  @Length(3, 20)
  @IsString()
    name: string;

  @IsString()
  @MinLength(7)
    password: string;
}

export default RegisterDto;

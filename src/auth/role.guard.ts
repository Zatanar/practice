import {
  CanActivate, ExecutionContext, mixin, Type,
} from '@nestjs/common';

import Role from '../users/role.enum';
import { JwtAuthGuard } from './jwt-auth.guard';
import RequestWithUser from './interfaces/requestWithUser.interface';

const RoleGuard = (role: Role): Type<CanActivate> => {
  class RoleGuardMixin extends JwtAuthGuard {
    async canActivate(context: ExecutionContext) {
      await super.canActivate(context);

      const request = context.switchToHttp().getRequest<RequestWithUser>();
      const { user } = request;

      return user?.roles.includes(role);
    }
  }

  return mixin(RoleGuardMixin);
};

export default RoleGuard;

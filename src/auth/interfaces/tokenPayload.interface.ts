import Role from '../../users/role.enum';

interface TokenPayload {
  username: string
  // user id
  sub: {
    userId: string;
    roles:Role[];
  };
}

export default TokenPayload;

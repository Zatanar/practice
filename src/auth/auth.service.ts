import {
  HttpException, HttpStatus, Injectable,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../users/users.service';
import { UserEntity } from '../users/entities/user.entity';
import { RegisterDto } from './dto/register.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  public async register(registrationData: RegisterDto): Promise<UserEntity> {
    const hashedPassword = await bcrypt.hash(registrationData.password, 10);

    const createdUser = await this.usersService.createUser({
      ...registrationData,
      password: hashedPassword,
    });

    if (!createdUser) {
      throw new HttpException('No user found', HttpStatus.NOT_FOUND);
    }
    return createdUser;
  }

  public login(user: UserEntity): string {
    const payload = {
      username: user.name,
      sub: {
        userId: user.id,
        roles: user.roles,
      },
    };
    return this.jwtService.sign(payload);
  }

  public async getAuthenticatedUser(name: string, plainTextPassword: string): Promise<UserEntity> {
    try {
      const user = await this.usersService.findUserByName(name);
      await this.verifyPassword(plainTextPassword, user.password);
      return user;
    } catch (error) {
      throw new HttpException('Wrong data', HttpStatus.BAD_REQUEST);
    }
  }

  private async verifyPassword(plainTextPassword: string, hashedPassword: string): Promise<void> {
    const isPasswordMatching = await bcrypt.compare(
      plainTextPassword,
      hashedPassword,
    );
    if (!isPasswordMatching) {
      throw new HttpException('Wrong data', HttpStatus.BAD_REQUEST);
    }
  }
}

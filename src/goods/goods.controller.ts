import {
  Controller, Get, Post, Body,
} from '@nestjs/common';
import { GoodsService } from './goods.service';
import { CreateGoodInput } from './dto/create-good.input';

@Controller('goods')
export class GoodsController {
  constructor(private goodsService: GoodsService) {}

  @Get()
  async findAllGoods() {
    return this.goodsService.findAllGoods();
  }

  @Post()
  async createGood(@Body() input: CreateGoodInput) {
    return this.goodsService.createGood(input);
  }
}

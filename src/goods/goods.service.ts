import { Injectable } from '@nestjs/common';
import { GoodEntity } from './entities/good.entity';
import { CreateGoodInput } from './dto/create-good.input';
import { UpdateGoodInput } from './dto/update-good.input';

@Injectable()
export class GoodsService {
  // id is uuid
  async findGoodById(id:string): Promise<GoodEntity> {
    return GoodEntity.query().findById(id).withGraphFetched('user');
  }

  async findAllGoods(): Promise<GoodEntity[]> {
    return GoodEntity.query().withGraphFetched('user');
  }

  async createGood(input: CreateGoodInput): Promise<GoodEntity> {
    return GoodEntity.query().insert(input);
  }

  // id is uuid
  async deleteGood(id: string): Promise<number> {
    return GoodEntity.query().deleteById(id);
  }

  async updateGood(input: UpdateGoodInput): Promise<number> {
    return GoodEntity.query().where({ id: input.id }).update(input);
  }
}

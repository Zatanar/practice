import {
  Field, ID, Float, InputType,
} from '@nestjs/graphql';
import { IsUUID, Min } from 'class-validator';

@InputType()
export class CreateGoodInput {
  // user_id is uuid
  @IsUUID('4')
  @Field(() => ID)
    userId: string;

  @Min(50)
  @Field(() => Float)
    cost: number;
}

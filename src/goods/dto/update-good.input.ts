import {
  Field, ID, InputType, PartialType,
} from '@nestjs/graphql';
import { IsUUID } from 'class-validator';
import { CreateGoodInput } from './create-good.input';

@InputType()
export class UpdateGoodInput extends PartialType(CreateGoodInput) {
  @IsUUID('4')
  @Field(() => ID)
    id: string;
}

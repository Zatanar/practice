import { Module } from '@nestjs/common';
import { GoodsController } from './goods.controller';
import { GoodsService } from './goods.service';
import { GoodsResolver } from './goods.resolver';
import { UsersService } from '../users/users.service';

@Module({
  controllers: [GoodsController],
  providers: [GoodsService, GoodsResolver, UsersService],
  exports: [],
})
export class GoodsModule {
}

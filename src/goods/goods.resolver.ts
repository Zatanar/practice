import {
  Args, ID, Int, Mutation, Query, Resolver,
} from '@nestjs/graphql';
import { ParseUUIDPipe } from '@nestjs/common';
import { GoodEntity } from './entities/good.entity';
import { GoodsService } from './goods.service';
import { CreateGoodInput } from './dto/create-good.input';
import { UpdateGoodInput } from './dto/update-good.input';

@Resolver(GoodEntity)
export class GoodsResolver {
  constructor(private readonly goodsService: GoodsService) {}

  @Query(() => GoodEntity)
  async neverUseFunction() {
    return 0;
  }

  // id is uuid
  // @Query(() => GoodEntity)
  // async findGoodById(@Args('id', { type: () => ID }, ParseUUIDPipe) id:string): Promise<GoodEntity> {
  //   return this.goodsService.findGoodById(id);
  // }

  // @Query(() => [GoodEntity])
  // async findAllGoods(): Promise<GoodEntity[]> {
  //   return this.goodsService.findAllGoods();
  // }

  @Mutation(() => GoodEntity)
  async createGood(@Args('input') input: CreateGoodInput) :Promise<GoodEntity> {
    return this.goodsService.createGood(input);
  }

  // id is uuid
  @Mutation(() => Int)
  async deleteGood(@Args('id', { type: () => ID }, ParseUUIDPipe) id:string): Promise<number> {
    return this.goodsService.deleteGood(id);
  }

  @Mutation(() => Int)
  async updateGood(@Args('update') input: UpdateGoodInput) :Promise<number> {
    return this.goodsService.updateGood(input);
  }
}

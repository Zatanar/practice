import { Model } from 'objection';
import {
  Field, ID, Float, ObjectType,
} from '@nestjs/graphql';
import { UserEntity } from '../../users/entities/user.entity';

@ObjectType()
export class GoodEntity extends Model {
  static get tableName() {
    return 'goods';
  }

  // id is uuid
  @Field((type) => ID)
    id:string;

  // user_id is uuid
  @Field((type) => ID)
    userId: string;

  @Field((type) => UserEntity)
    user: UserEntity;

  @Field((type) => Float)
    cost: number;

  static relationMappings = () => ({
    user: {
      relation: Model.BelongsToOneRelation,
      modelClass: UserEntity,
      join: {
        from: 'goods.userId',
        to: 'users.id',
      },
    },
  });
}

import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';

async function emitSchema() {
  const app = await NestFactory.create(AppModule);
  await app.init();
}

emitSchema()
  .then(() => process.exit(0));

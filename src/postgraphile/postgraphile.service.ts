import { makeSchemaAndPlugin } from 'postgraphile-apollo-server';
import { Pool, PoolConfig } from 'pg';
import * as ConnectionFilterPlugin from 'postgraphile-plugin-connection-filter';
import * as simplify from '@graphile-contrib/pg-simplify-inflector';
import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { namePlugin, PgSmallNumericToFloatPlugin } from './pg-plugins';

@Injectable()
export class PostgraphileService {
  constructor(
    private readonly config: ConfigService,
  ) {}

  async schema() {
    const POSTGRES_PATH = this.config.get('POSTGRES_PATH');

    const dbConf: PoolConfig = {
      connectionString: POSTGRES_PATH,
      min: 2,
      idleTimeoutMillis: 30000,
    };

    const pgPool = new Pool(dbConf);

    const appendPlugins = [
      simplify,
      namePlugin,
      ConnectionFilterPlugin,
      PgSmallNumericToFloatPlugin,
    ];

    return makeSchemaAndPlugin(
      pgPool,
      'public', // PostgreSQL schema to use
      {
        appendPlugins,
        // PostGraphile options, see:
        // https://www.graphile.org/postgraphile/usage-library/
        disableDefaultMutations: true,
        watchPg: true,
        dynamicJson: true, // By default, JSON and JSONB fields are presented as strings (JSON encoded),
        graphileBuildOptions: {
          pgDeletedColumnName: 'deleted_at',
          pgDeletedRelations: true,
          connectionFilterRelations: true,
          /*
           * Uncomment if you want simple collections to lose the 'List' suffix
           * (and connections to gain a 'Connection' suffix).
           */
          pgOmitListSuffix: true,
          /*
           * Uncomment if you want 'userPatch' instead of 'patch' in update
           * mutations.
           */
          // pgSimplifyPatch: false,
          /*
           * Uncomment if you want 'allUsers' instead of 'users' at root level.
           */
          // pgSimplifyAllRows: false,
          /*
           * Uncomment if you want primary key queries and mutations to have
           * `ById` (or similar) suffix; and the `nodeId` queries/mutations
           * to lose their `ByNodeId` suffix.
           */
          // pgShortPk: true,
        },
        simpleCollections: 'both',
        pgOmitListSuffix: true,
        pgSettings: async (req: any) => ({}),
      },
    );
  }
}

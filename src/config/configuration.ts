export default () => ({
  POSTGRES_PATH: process.env.POSTGRES_PATH,
  APP_PORT: process.env.APP_PORT,
  NODE_ENV: process.env.NODE_ENV,
  JWT_EXPIRATION_TIME: process.env.JWT_EXPIRATION_TIME,
  JWT_SECRET: process.env.SECRET,
  JWT_EXPIRES: process.env.JWT_EXPIRES,

});

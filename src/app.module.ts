import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { ConfigModule } from '@nestjs/config';
import { GraphQLSchema } from 'graphql';
import { stitchSchemas } from 'graphql-tools';
import configuration from './config/configuration';
import { DatabaseModule } from './database/database.module';
import { UsersModule } from './users/users.module';
import { GoodsModule } from './goods/goods.module';
import { PostgraphileService } from './postgraphile/postgraphile.service';
import { PostgraphileModule } from './postgraphile/postgraphile.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    DatabaseModule,
    UsersModule,
    GoodsModule,
    AuthModule,
    GraphQLModule.forRootAsync({
      imports: [PostgraphileModule],
      inject: [PostgraphileService],
      useFactory: async (postgraphileService: PostgraphileService) => {
        const pgSchema = await postgraphileService.schema();
        const typeMergingOptions: any = {
          validationSettings: {
            validationLevel: 'off',
          },
        };
        return {
          plugins: [pgSchema.plugin],
          autoSchemaFile: 'schema.gql',
          sortSchema: true,
          transformAutoSchemaFile: true,
          transformSchema: async (schema: GraphQLSchema) => stitchSchemas({
            subschemas: [
              { schema },
              { schema: pgSchema.schema },
            ],
            typeMergingOptions,
          }),
          installSubscriptionHandlers: true,
          context: ({ req, connection }) => (connection ? { req: { headers: connection.context } } : { req }),
          uploads: false, // 👉 https://www.apollographql.com/docs/apollo-server/data/file-uploads/#uploads-in-node-14-and-later
        };
      },
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
  ],
  providers: [],
})
export class AppModule {}
